import os
import re
import subprocess


def syn2svg(syn_file, pro_file, links_file, **kwargs):
    """Function to convert .syn-files (AIMMS Syntax Diagrams) to .svg-files

    Args:
        syn_file (str) -- path to .syn-file
        pro_file (str) -- path to .pro-file (file containing custom PostScript commands)
        links_file (str) -- path to 'syntax_diagram_links.txt'

    Kwargs:
        svg_dir (str) -- path to directory to store .svg-files (default None)
        keep_ps_file (bool) -- keep the intermediate .ps-file (default False)
        ps_dir (str) -- path to directory to store .ps-files (default None)

    Dependencies:
        GPL Ghostscript 9.52 or higher (with standard bundle 'ps2pdf')
        Inkscape 0.92.4 or higher (from command line)

    """
    # Extract keyword arguments
    svg_dir = kwargs.get('svg_dir')
    keep_ps_file = kwargs.get('keep_ps_file', False)
    ps_dir = kwargs.get('ps_dir')

    # Check arguments
    assert os.path.isfile(syn_file), 'path to .syn-file does not exist.'
    assert os.path.isfile(pro_file), 'path to .pro-file does not exist.'
    assert os.path.isfile(links_file), 'path to \'syntax_diagram_links.txt\' does not exist.'

    # Load 'syntax_diagram_links.txt'
    with open(links_file, 'r', encoding='latin-1') as f:
        try:
            syntax_diagram_links = eval(f.read())
        except SyntaxError:
            raise Exception('content of \'syntax_diagram_links.txt\' is not a dictionary.')
    if svg_dir:
        assert os.path.isdir(svg_dir), 'path to directory to store .svg-files does not exist.'
    if ps_dir:
        assert os.path.isdir(ps_dir), 'path to directory to store .ps-files does not exist.'
    if keep_ps_file:
        assert isinstance(keep_ps_file, bool), 'keyword argument \'keep_ps_file\' should be True of False.'

    # Load PostScript instructions from .pro-file
    with open(pro_file, 'r', encoding='latin-1') as f:
        ps_instructions = f.read()
    # Load .syn-file
    with open(syn_file, 'r', encoding='latin-1') as f:
        syn_file_code = f.read()

    # Find BoundingBox values
    bounding_box = re.search(r'%%BoundingBox:\s*(\S+)\s+(\S+)\s+(\S+)\s+(\S+)', syn_file_code)
    if bounding_box:
        # Multiply BoundingBox values by 10
        bounding_box_mod = r'%%BoundingBox: ' + r' '.join([str(10 * int(bounding_box.group(i))) for i in range(1, 5)])
        # Replace BoundingBox values
        syn_file_code = syn_file_code.replace(bounding_box.group(0), bounding_box_mod)

    # Find part of prolog after comments
    prolog = re.search(r'%%EndComments(.*)%%EndProlog', syn_file_code, re.DOTALL)
    if prolog:
        # Add PostScript instructions to prolog
        prolog_mod = r'%%EndComments' + '\n\n' + ps_instructions + prolog.group(1) + r'%%EndProlog'
        # Replace part of prolog after comments
        syn_file_code = syn_file_code.replace(prolog.group(0), prolog_mod)

    # Save syn_file_code as .ps-file
    if ps_dir:
        ps_file = os.path.join(ps_dir, os.path.basename(syn_file).replace('.syn', '.ps'))
    else:
        ps_file = syn_file.replace('.syn', '.ps')
    with open(ps_file, 'w') as f:
        f.write(syn_file_code)

    # Convert .ps-file to .pdf-file using Ghostscript
    pdf_file = ps_file.replace('.ps', '.pdf')
    try:
        subprocess.run(['ps2pdf', '-dEPSCrop', ps_file])
    except (OSError, ValueError, subprocess.SubprocessError):
        pass

    # Convert .pdf-file to .svg-file and crop using Inkscape
    if svg_dir:
        svg_file = os.path.join(svg_dir, os.path.basename(syn_file).replace('.syn', '.svg'))
    else:
        svg_file = syn_file.replace('.syn', '.svg')
    try:
        subprocess.run(['inkscape', pdf_file, '--export-area-drawing', '--export-plain-svg=' + svg_file])
    except (OSError, ValueError, subprocess.SubprocessError):
        pass

    # Delete .ps-file if desired
    if keep_ps_file is False:
        os.remove(ps_file)

    # Delete .pdf-file
    os.remove(pdf_file)

    # Load and modify .svg-file to clean up formatting and add hyperlinks
    with open(svg_file, 'r', encoding='latin-1') as f:
        svg_file_code = f.read()

    # Iterate over <text> tags in .svg-file code
    for text_tag in re.finditer(r'<text.+?</text>', svg_file_code, re.DOTALL):
        if text_tag:
            # Find <tspan> tag
            tspan_tag = re.search(r'<tspan.+?x=\"(.+?)\".*?>(.+?)</tspan>', text_tag.group(0), re.DOTALL)
            if tspan_tag:
                # Define modified <tspan> tag
                tspan_tag_mod = tspan_tag.group(0)
                # Modify <tspan> tag
                if tspan_tag.group(1) != '0':
                    tspan_tag_mod = tspan_tag_mod.replace(tspan_tag.group(1), '0')
                if tspan_tag.group(2) in syntax_diagram_links:
                    hyperlink = syntax_diagram_links[tspan_tag.group(2)]
                    a_tag = '<a href="' + hyperlink + '">' + tspan_tag.group(2) + '</a>'
                    tspan_tag_mod = tspan_tag_mod.replace(tspan_tag.group(2), a_tag)
                # Modify <text> tag
                text_tag_mod = text_tag.group(0).replace(tspan_tag.group(0), tspan_tag_mod)
                if 'LucidaSans-Typewriter' in text_tag_mod:
                    text_tag_mod = text_tag_mod.replace('\'Lucida Sans\'', '\'Courier New\'')
                else:
                    text_tag_mod = text_tag_mod.replace('font-size:12px', 'font-size:11px')
                # Modify .svg-file code
                svg_file_code = svg_file_code.replace(text_tag.group(0), text_tag_mod)

        # Save modified .svg-file code
        with open(svg_file, 'w') as f:
            f.write(svg_file_code)
