from datetime import datetime
import os
import syndia

# Conversion started...
start_time = datetime.now()
print(str(start_time) + ' -- Conversion started...')

# # 1. Convert single .syn-file
# main_dir = r'...'
# syn_file = os.path.join(main_dir, r'...')
# pro_file = os.path.abspath(r'syntaxdiagram.pro')
# links_file = os.path.abspath(r'syntax_diagram_links.txt')
# ps_dir = main_dir
# svg_dir = main_dir
#
# n_converted = 0
# try:
#     # Run conversion
#     syndia.syn2svg(syn_file, pro_file, links_file, keep_ps_file=True, ps_dir=ps_dir, svg_dir=svg_dir)
#     # Counter
#     n_converted += 1
#     # Print
#     print('File \'' + os.path.basename(syn_file) + '\' converted!')
# except Exception as err:
#     # Print
#     print('File \'' + os.path.basename(syn_file) + '\' did not pass. Error message: ' + str(err))
#     pass

# 2. Convert multiple .syn-files
main_dir = r'...'
syn_dir = os.path.join(main_dir, 'syn')
pro_file = os.path.abspath(r'syntaxdiagram.pro')
links_file = os.path.abspath(r'syntax_diagram_links.txt')
ps_dir = os.path.join(main_dir, 'ps')
svg_dir = os.path.join(main_dir, 'svg')

# Create list of .syn-files
syn_files_list = []
for root, dirs, files in os.walk(syn_dir):
    for file in files:
        if file.endswith('.syn'):
            syn_files_list.append(os.path.join(root, file))

# Iterate over .syn-files to convert them to .svg-files
n_converted = 0
for syn_file in syn_files_list:
    try:
        # Run conversion
        syndia.syn2svg(syn_file, pro_file, links_file, keep_ps_file=True, ps_dir=ps_dir, svg_dir=svg_dir)
        # Counter
        n_converted += 1
        # Print
        print('File \'' + os.path.basename(syn_file) + '\' converted!')
    except Exception as err:
        # Print
        print('File \'' + os.path.basename(syn_file) + '\' did not pass. Error message: ' + str(err))
        pass

# Conversion completed!
end_time = datetime.now()
print(str(end_time) + ' -- Conversion completed!')
print('Time elapsed = ' + str(end_time - start_time))
print('Files converted = ' + str(n_converted))
