import os
import re
from shutil import which
import subprocess


def check_path(path, func_name):
    """Check if the path to a file or directory exists. If it doesn't, an exception is raised.

    Parameters
    ----------
    path : str
        Path to file or directory.
    func_name : str
        Name of the function 'check_path' is called from.

    """
    if not os.path.isfile(path) and not os.path.isdir(path):
        raise Exception(
            'pspic.{}: path given in arg \'tex_doc\' does not exist.'.format(func_name))


def extract(tex_doc):
    """Function to extract one or more pspicture environments from a LaTeX-document.

    Parameters
    ----------
    tex_doc : str
        Path to LaTeX-document.

    Returns
    -------
    pspic_matches : list of str
        List of all pspicture environments found in the LaTeX-document.

    """
    # Check 'tex_doc' arg
    check_path(tex_doc, 'extract')

    # Read LaTeX-document
    with open(tex_doc, 'r', encoding='latin-1') as f:
        tex_code = f.read()

    # Define regex
    pspic_regex = r'((?:\\ps\w+{[^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*}([\s\S]*?))*' \
                  r'\\begin{pspicture}[\s\S]+?\\end{pspicture})'

    # Find all non-overlapping matches of 'pspic_regex' in 'tex_code', as a list of strings
    def str_repl(matchobj):
        if matchobj.group(2):
            return re.sub(re.escape(matchobj.group(2)),
                          re.sub(r'\\begin{\w+?}', r'', matchobj.group(2)),
                          matchobj.group(0))
        else:
            return matchobj.group(1)
    pspic_matches = [match[0] for match in re.findall(pspic_regex, re.sub(pspic_regex, str_repl, tex_code))]

    return pspic_matches


def build_texdoc(pspic_env, preamble=''):
    """Function to build a LaTeX-document around a pspicture environment.

    Parameters
    ----------
    pspic_env : str
        pspicture environment.
    preamble : str, optional
        Preamble of the LaTeX-document.

    Returns
    -------
    latex_code : list of str
        Code of the LaTeX-document.

    """
    # Check 'pspic_env' arg
    if not isinstance(pspic_env, str):
        raise ValueError(
            'pspic.build_texdoc: arg \'pspic_env\' must be a string.')
    if r'\begin{pspicture}' not in pspic_env or r'\end{pspicture}' not in pspic_env:
        raise Exception(
            'pspic.build_texdoc: arg \'pspic_env\' is not a valid pspicture environment.')

    # Check 'preamble' kwarg
    if not isinstance(preamble, str):
        raise ValueError(
            'pspic.build_texdoc: kwarg \'preamble\' must be a string.')
    if not preamble:
        preamble = r'\documentclass{article}' + '\n\n'
        preamble += r'\usepackage{pstricks}' + '\n\n'
        preamble += r'\pagenumbering{gobble}'

    # Build document
    latex_code = preamble + '\n\n'
    latex_code += r'\begin{document}' + '\n\n'
    latex_code += r'\begin{figure}' + '\n'
    latex_code += pspic_env + '\n'
    latex_code += r'\end{figure}' + '\n\n'
    latex_code += r'\end{document}'

    return latex_code


def convert(tex_doc, target_dir='', export_type='png', export_dpi=300, export_area_drawing=True):
    """Function to convert a pspicture environment in a LaTeX-document to a specified image file format.

    Parameters
    ----------
    tex_doc : str
        Path to LaTeX-document.
    target_dir : str, optional
        Path to target directory.
    export_type : str, optional
        Export image file format. Possible values: EPS, PDF, PNG, PS, SVG (the default is 'png').
    export_dpi : int, optional
        The resolution used for PNG export (the default is 300).
    export_area_drawing : bool, optional
        Exported area is equal to the bounding box of the drawing (the default is True).

    Notes
    -----
    The following software must be installed:
        * MiKTeX 2.9.7440 or higher (with any custom packages/fonts as used by the LaTeX-document installed)
        * Inkscape 1.0 or higher

    The following programs must be callable from the command line:
        * latex
        * dvips
        * ps2pdf
        * inkscape

    """
    # Check software requirements
    program_reqs = ['latex', 'dvips', 'ps2pdf', 'inkscape']
    for prog in program_reqs:
        if not which(prog):
            raise Exception(
                'pspic.convert: executable \'{}\' was not found in PATH.'.format(prog))

    # Check 'tex_doc' arg
    check_path(tex_doc, 'convert')

    # Check 'target_dir' kwarg
    if not target_dir:
        target_dir = os.path.dirname(tex_doc)
    check_path(target_dir, 'convert')

    # Check 'export_type' kwarg
    export_type_options = ['eps', 'pdf', 'png', 'ps', 'svg']
    export_type = export_type.lower()
    if not isinstance(export_type, str):
        raise ValueError(
            'pspic.convert: kwarg \'export_type\' must be a string.')
    if export_type not in export_type_options:
        raise Exception(
            'pspic.convert: output format \'{}\' is not supported -- '.format(export_type)
            + 'supported output formats: ' + ', '.join(export_type_options) + '.')

    # Check 'export_dpi' kwarg
    if not isinstance(export_dpi, int):
        raise ValueError(
            'pspic.convert: kwarg \'export_dpi\' must be an integer.')

    # Check 'export_area_drawing' kwarg
    if not isinstance(export_area_drawing, bool):
        raise ValueError(
            'pspic.convert: kwarg \'export_area_drawing\' must be True or False.')

    # Directory name of LaTeX-document
    dir_name = os.path.dirname(tex_doc)

    # Compile LaTeX document
    max_runs = 15
    prev_aux = None
    runs_left = max_runs
    while runs_left:
        try:
            subprocess.run(['latex', tex_doc], cwd=dir_name,
                           stdout=subprocess.DEVNULL,
                           stderr=subprocess.DEVNULL)
            # subprocess.run(['latex', tex_doc], cwd=dir_name)
            # Check LaTeX auxiliary file
            with open(tex_doc.replace('.tex', '.aux'), 'r', encoding='latin-1') as f:
                aux = f.read()
            if aux == prev_aux:
                break
            prev_aux = aux
            runs_left -= 1
        except (OSError, ValueError, subprocess.SubprocessError):
            pass
    else:
        raise RuntimeError(
            'pspic.convert: maximum number of runs ({}) without a stable .aux-file reached.'.format(max_runs))
    try:
        # DVI to PS
        subprocess.run(['dvips', '-P', 'pdf', tex_doc.replace('.tex', '.dvi')], cwd=dir_name,
                       stdout=subprocess.DEVNULL,
                       stderr=subprocess.DEVNULL)
        # subprocess.run(['dvips', '-P', 'pdf', tex_doc.replace('.tex', '.dvi')], cwd=dir_name)
        # PS to PDF
        subprocess.run(['ps2pdf', tex_doc.replace('.tex', '.ps')], cwd=dir_name,
                       stdout=subprocess.DEVNULL,
                       stderr=subprocess.DEVNULL)
        # subprocess.run(['ps2pdf', tex_doc.replace('.tex', '.ps')], cwd=dir_name)
    except (OSError, ValueError, subprocess.SubprocessError):
        pass

    # Remove auxiliary and intermediate LaTeX files
    tex_exts = ['.aml', '.aux', '.dvi', '.log', '.ps', '.txt']
    for ext in tex_exts:
        tex_file = tex_doc.replace('.tex', ext)
        if os.path.isfile(tex_file):
            os.remove(tex_file)

    # Convert resulting PDF to specified image file format
    export_filename = os.path.join(target_dir, os.path.basename(tex_doc).replace('.tex', '.' + export_type))
    inkscape_args = ['inkscape',
                     '--pdf-poppler',
                     '--export-dpi=' + str(export_dpi),
                     '--export-background-opacity=0.0',
                     '--export-area-drawing',
                     '--export-filename=' + export_filename,
                     tex_doc.replace('.tex', '.pdf')]
    if not export_area_drawing:
        inkscape_args.remove('--export-area-drawing')
    try:
        subprocess.run(inkscape_args, cwd=dir_name)
    except (OSError, ValueError, subprocess.SubprocessError):
        pass

    # Remove intermediate PDF file
    if export_type != 'pdf':
        pdf_file = tex_doc.replace('.tex', '.pdf')
        if os.path.isfile(pdf_file):
            os.remove(pdf_file)

    # Print
    if os.path.isfile(export_filename):
        if export_type == 'png':
            print(export_filename + ' (DPI {}) was successfully created!'.format(str(export_dpi)))
        else:
            print(export_filename + ' was successfully created!')
    else:
        print('Conversion of pspicture environment in the LaTeX-document was unsuccessful...')
