userdict begin

/begindiagram {
gsave 
1 setlinejoin
0.4 setlinewidth
newpath 0 0 moveto
syntaxdict begin
/hwidth exch def
} def

/enddiagram {
line 0 rightarrow
end 
stroke
showpage
grestore
} def

/diagramcontinue {
line currentpoint 
gsave newpath moveto [4 2] 0 setdash 24 0 rlineto stroke grestore
} def

/continuediagram {
gsave newpath moveto [4 2] 0 setdash 24 0 rlineto currentpoint stroke grestore
moveto
} def

end

/syntaxdict 40 dict def

syntaxdict begin

/bheight 20 def
/radius 10 def
/textdown -4 def
/hhwidth { hwidth 2 div } def
/hbheight { bheight 2 div } def
/leveldim 30 def
/arrowgraylev 0 def

/beginlevel {
20 dict begin
line currentpoint
/pointy exch def
/pointx exch def
/levelwidth 0 def
/hwidth exch def
} def

/endlevel { end } def

/arrowgray { currentgray 1 eq {1} {arrowgraylev} ifelse } def
/negarrowgray { currentgray 1 eq {1} {1 arrowgraylev sub} ifelse } def

/arrow { setgray currentpoint newpath moveto
         -5 2 rlineto 0 -4 rlineto closepath fill } def
/rightarrow { gsave arrow grestore } def
/leftarrow { gsave 180 rotate arrow grestore } def
/uparrow { gsave 90 rotate arrow grestore } def
/downarrow { gsave -90 rotate arrow grestore } def
/wline { 0 rlineto } def
/line { hwidth wline } def

/max { 2 copy gt {pop} {exch pop} ifelse } def
/sign { dup 0 eq {pop 0} {dup abs div} ifelse } def

/popfour { 4 {pop} repeat } def
/addpoints { exch 3 1 roll add 3 1 roll add exch } def
/rarcto { 5 -2 roll currentpoint addpoints
5 -2 roll currentpoint addpoints 5 -1 roll arcto } def

/nonterminal {
line arrowgray rightarrow
/LucidaSans-Italic findfont 12 scalefont setfont
dup /nontermtext exch def
/rightmove exch stringwidth pop radius add def
0 hbheight rlineto
rightmove 0 rlineto
currentpoint /nt@ury exch def /nt@urx exch def
0 hbheight neg rlineto
0 hbheight neg rlineto
rightmove neg 0 rlineto
currentpoint /nt@lly exch def /nt@llx exch def
0 hbheight rlineto    
radius 2 div textdown rmoveto
syntaxdict /nohyref known {nontermtext show}
{0.825 0.175 0.175 setrgbcolor nontermtext show 0 setgray} ifelse
radius 2 div textdown neg rmoveto
syntaxdict /nohyref known not { /linkref
nontermtext length 7 add string dup dup (syntax.) exch copy pop
7 nontermtext putinterval cvn def
[ /Subtype /Link /Dest linkref /Border [0 0 0] /Color
[1 0 0] /Rect [nt@llx nt@lly nt@urx nt@ury] /ANN pdfmark } if
negarrowgray leftarrow
} def

/terminal {
line arrowgray rightarrow
/LucidaSans-Typewriter findfont 12 scalefont setfont
dup /rightmove exch stringwidth pop radius max def  
0 hbheight hbheight hbheight radius rarcto popfour
rightmove 0 rightmove hbheight neg radius rarcto popfour
0 hbheight radius sub neg rlineto
0 hbheight neg hbheight neg hbheight neg radius rarcto popfour
rightmove neg 0 rightmove neg hbheight radius rarcto popfour
0 hbheight radius sub rlineto
dup /textright exch stringwidth pop rightmove sub neg radius add 2 div def
textright textdown rmoveto
show
textright textdown neg rmoveto
negarrowgray leftarrow
} def

/levelverticalgo {
/levelvmove exch leveldim mul def
/levelsign levelvmove sign neg def
levelsign 0 eq 
   {levelwidth 0 ne {radius 0 rlineto} if}
   {gsave 90 levelsign mul rotate negarrowgray arrow grestore
    0 levelvmove radius levelvmove radius rarcto popfour} ifelse
} def

/levelverticalreturn {
levelsign 0 eq
   {levelwidth 0 eq 
        {/levelwidth levelcurrentwidth radius 2 mul sub def}
        {radius levelhmove add 0 rlineto} ifelse}
   {radius levelhmove add 0 
    radius levelhmove add levelvmove neg radius rarcto popfour
    0 levelvmove abs radius sub levelsign mul rlineto
    gsave 90 levelsign mul rotate arrowgray arrow grestore} ifelse
levelwidth 0 eq {/levelwidth levelcurrentwidth def} if
} def

/levelitem {
pointx pointy moveto
/levelproc exch def 
levelverticalgo 
gsave 1 setgray
   newpath 20 10000 moveto levelproc line 
   pathbbox 2 {pop exch} repeat sub /levelcurrentwidth exch def
grestore
/levelhmove levelwidth 0 eq 
     {0} {levelwidth levelcurrentwidth sub 2 div} ifelse def
levelhmove 0 rlineto levelproc 
line levelverticalreturn
} def

/hfill {
pointx pointy moveto
levelwidth radius 2 mul add 0 rlineto
} def

/repeater {
levelwidth 0 ne
  {/levelwidth currentpoint pop pointx sub radius 2 mul sub def} if
/arrowgraylev 1 def
levelitem
/arrowgraylev 0 def
} def

end