# tex_command_extractor - Extract all TeX-commands from TeX-files

import os
import re

# Define path to TeX-files directory
path2dir = r'...'

# Create list of TeX-files
tex_files = []
for root, dirs, files in os.walk(path2dir):
    for file in files:
        if file.endswith('.tex'):
            tex_files.append(os.path.join(root, file))


def remove_mult_args(strng):
    def str_repl(matchobj):
        if matchobj.group(1) == r'{' and matchobj.group(2) == r'}':
            return r'{}'
        if matchobj.group(1) == r'[' and matchobj.group(2) == r']':
            return r'[]'
        else:
            return matchobj.group(0)
    return re.sub(r'([\[{])[^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*([\]}])', str_repl, strng, re.DOTALL)


def command_extract(strng):
    # Define return variable
    output = []

    # Define regular expression
    tex_command_regex = r'({)?\\([a-zA-Z0-9]+(?(1)\\[a-zA-Z0-9]+)*)' \
                        r'(?:' \
                            r'(?:' \
                                r'{([^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*)}' \
                                r'|' \
                                r'\[(.*?)\]' \
                                r'|' \
                                r'(?(1)\s(.*?)(}))' \
                            r')' \
                            r'(?(6)|((?:[\[{][^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*[\]}])*))' \
                            r'|' \
                            r'(?(1)(?(6)|(})))' \
                        r')' \
                        r'(?(8)|(?(1)(?(6)|})))?'

    # Iterate over TeX-commands in string
    for tex_command_obj in re.finditer(tex_command_regex, strng):

        # Parameterize regex groups
        full_match = tex_command_obj.group(0)
        open_curl_brack = tex_command_obj.group(1)
        tex_command = tex_command_obj.group(2)
        curl_arg = tex_command_obj.group(3)
        square_arg = tex_command_obj.group(4)
        open_curl_content = tex_command_obj.group(5)
        mult_args = tex_command_obj.group(7)
        close_curl_command = tex_command_obj.group(8)

        if mult_args:
            for arg_obj in re.finditer(r'[\[{]([^}{]*(?:{[^}{]*(?:{[^}{]*}[^}{]*)*}[^}{]*)*)[\]}]', mult_args):
                output += command_extract(arg_obj.group(1))
            mult_args_repl = remove_mult_args(mult_args)
            full_match = full_match.replace(mult_args, mult_args_repl)

        if open_curl_brack:
            if open_curl_content:
                output.append(' '.join(full_match.replace(open_curl_content, '').split()))
            elif close_curl_command:
                if full_match.count('\\') == 1:
                    output.append(' '.join(full_match.split()))
                else:
                    output += command_extract(full_match[1:])
            else:
                output += command_extract(full_match[1:])
        elif not curl_arg and not square_arg:
            output.append(' '.join(full_match.split()))
        elif curl_arg or square_arg:
            if tex_command == 'begin' or tex_command == 'end':
                output.append(' '.join(full_match.split()))
            else:
                if curl_arg:
                    output.append(' '.join(full_match.replace(curl_arg, '').split()))
                elif square_arg:
                    output.append(' '.join(full_match.replace(square_arg, '').split()))
        else:
            output.append(' '.join(full_match.split()))

    return output


# Define variable to collect clean output commands
commands_output = []

# Iterate over list of files
for file in tex_files:

    # Read code
    with open(file, 'r', encoding='latin-1') as f:
        function_code = f.read()

    # Extract TeX-commands from code and add them to the list
    commands_output += command_extract(function_code)

# Remove duplicates from 'commands_output' list and sort alphabetically
commands_output = sorted(set(commands_output))

# Print
for command in commands_output:
    print(command)
